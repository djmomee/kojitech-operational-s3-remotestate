
resource "aws_s3_bucket" "statefile_bucket" {
  count = length(var.bucket_name)
  bucket = var.bucket_name[count.index]
}


resource "aws_s3_bucket_acl" "statefile_bucket" {
  count = length(var.bucket_name)
  bucket = aws_s3_bucket.statefile_bucket[count.index].id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "versioning_statefile_bucket" {
  count = length(var.bucket_name)
  bucket = aws_s3_bucket.statefile_bucket[count.index].id
  versioning_configuration {
    status = "Enabled"
  }
}

# create dynamodb

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "terraform-state-block"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}


